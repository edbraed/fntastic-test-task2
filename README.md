### Description: 
  
This test-task №2 for Fntastic game studio.  
Simple 3D scene on Unreal Engine.  
  
![image](screen.png "screen")  
  
You can launch 3 types of turtles
* 1 Self-confident turtle running towards the goal.
* 2 Weak turtle (periodically resting)
* 3 An insecure turtle who constantly wants to return home.

### Using content resources:  
turtle:  
https://sketchfab.com/3d-models/model-50a-hatchling-hawksbill-sea-turtle-e69458ef8176402d919df421e444da86
  
rock:  
https://sketchfab.com/3d-models/rock-17-free-rock-pack-vol3-112533f65f3d4848ae14d18ed59f0db2
  
cage (start spawner):  
https://sketchfab.com/3d-models/trap-cage-4b2ec2b622624f599a5de3e9e2664630
  
column (finish):  
https://sketchfab.com/3d-models/pillar-15ef23e2f6b1442cb698e2fa6618c928
  
water-materials:  
https://www.unrealengine.com/marketplace/en-US/product/water-materials
  
water-background sound:  
https://www.chosic.com/
  
water-step sound:  
https://freesound.org/people/nathanaelj83/sounds/145242/
  
mario-sounds:  
https://quicksounds.com/library/sounds/mario
  
dirt-texture (particle-sys):  
https://www.textures.com/download/DecalsDamageBrick0025/115946
