// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TurtleBase.generated.h"

UCLASS()
class FNTASTICTESTTASK2_API ATurtleBase : public AActor
{
	GENERATED_BODY()

private:
	FBodyInstance* m_physBody = nullptr;
	USkeletalMeshComponent* m_skelMesh = nullptr;
	float m_goalRadius = 1.0f;
	
public:	
	// Sets default values for this actor's properties
	ATurtleBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Turtle settings")
	float moveVelocity = 800.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Turtle settings")
	float torqVelocity = 10.5f;

	UFUNCTION(BlueprintCallable, Category = "Turtle settings")
	void SetGoalPosition(const FVector& position);

	UFUNCTION(BlueprintCallable, Category = "Turtle settings")
	void SetGoalRadius(float radius);

	UFUNCTION(BlueprintCallable, Category = "Turtle settings")
	void SetMoveToGoal(bool moving);

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Turtle settings")
	bool moveToGoal = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Turtle settings")
	FVector goalPosition = { 0.0f, 0.0f, 0.0f };

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
