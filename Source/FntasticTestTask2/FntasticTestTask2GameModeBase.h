// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FntasticTestTask2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FNTASTICTESTTASK2_API AFntasticTestTask2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
