// Fill out your copyright notice in the Description page of Project Settings.


#include "TurtleBase.h"
#include "Components/SkeletalMeshComponent.h"
#include "Animation/AnimSingleNodeInstance.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATurtleBase::ATurtleBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATurtleBase::BeginPlay()
{
	Super::BeginPlay();

	auto pSkelComp = this->GetComponentByClass(USkeletalMeshComponent::StaticClass());
	m_skelMesh = Cast<USkeletalMeshComponent>(pSkelComp);
	if (m_skelMesh)
		m_physBody = m_skelMesh->GetBodyInstance();
	else
		UE_LOG(LogTemp, Warning, TEXT("ED: No skeletal mesh!!"));
}

// Called every frame
void ATurtleBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (moveToGoal)
	{
		const auto rightword = m_skelMesh->GetRightVector();
		const auto upword = m_skelMesh->GetUpVector();
		const auto transform = m_physBody->GetUnrealWorldTransform();
		const auto toGoal = (goalPosition - transform.GetLocation()).GetUnsafeNormal();
		const FVector2D fv(rightword);
		const FVector2D gv(toGoal);
		const auto dp = FVector2D::DotProduct(gv, fv);
		const float angl = UKismetMathLibrary::Acos(dp / (fv.Size() * gv.Size())) - UKismetMathLibrary::GetPI();

		if (angl > 0.0654f)
			m_physBody->SetAngularVelocityInRadians(upword * torqVelocity, false);
		else if(angl < -0.0654f)
			m_physBody->SetAngularVelocityInRadians(upword * -torqVelocity, false);
		else
			m_physBody->SetAngularVelocityInRadians(FVector::ZeroVector, false);

		m_physBody->SetLinearVelocity(toGoal* moveVelocity, false);
	}
}

void ATurtleBase::SetGoalPosition(const FVector& position)
{
	m_physBody->SetLinearVelocity(FVector::ZeroVector, false);
	goalPosition = position;
}

void ATurtleBase::SetGoalRadius(float radius)
{
	m_goalRadius = radius;
}

void ATurtleBase::SetMoveToGoal(bool moving)
{ 
	moveToGoal = moving;
	if (!moveToGoal)
		m_physBody->SetLinearVelocity(FVector::ZeroVector, false);

	if (auto SingleNodeInstance = m_skelMesh->GetSingleNodeInstance())
		SingleNodeInstance->SetPlayRate(moveToGoal ? 1.0f : 0.2f);
}